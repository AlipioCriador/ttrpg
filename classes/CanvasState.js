var handleMouseMove, handleMouseDown, handleMouseUp, handleSelectStart;

var CanvasState = function(canvas)
{
	this.canvas = canvas;
	this.w = canvas.width;
	this.h = canvas.height;
	this.ctx = canvas.getContext('2d');



	this.valid = false; // Quando esse valor for falso, o canvas vai redesenhar tudo de novo
	this.pictures = []; // As coisas que vão ser desenhadas no canvas
	this.dragging = false; // Acompanha para ver se algo está sendo arrastado

	this.selection = null; // o Objeto que está sendo selecionado no momento

	// Valores que servem para arrastar a imagem relativamente a distância do mouse na imagem
	this.dragoffX = 0;
	this.dragoffY = 0;


	/**************       Events      **************/
	// Nos eventos tipo addEventListener, this lá significa
	// o canvas e não o CanvasState, como queremos usar o
	// CanvasState, we set up a variable to reference CanvasState
	var myC = this;

	canvas.addEventListener('selectstart', function(e)
	{
		e.preventDefault();
		return false;
	}, true);



	canvas.addEventListener('mousemove', function(e)
	{
		var mx = e.offsetX;
		var my = e.offsetY;


		myC.pictures.forEach(function(picture)
		{
			if(picture.scaleBoxes.length > 0)
			{
				for(var i = 0; i < picture.scaleBoxes.length; i++ )
				{
					var box = picture.scaleBoxes[i];
					if(picture.contains(mx, my, box.x, box.y, box.w, box.h))
					{
						console.log('contains');
						if(!i)
							canvas.style.cursor = 'nw-resize';
						else if (i === 1)
							canvas.style.cursor = 'se-resize';
						else if (i === 2)
							canvas.style.cursor = 'ne-resize';
						else
							canvas.style.cursor = 'sw-resize';

						return;
					}
					else
					{
						console.log('default');
						canvas.style.cursor = 'default';
					}

				};
			}
			
		});
		
				

		if(myC.dragging)
		{
			myC.selection.x = e.offsetX - myC.dragoffX;
			myC.selection.y = e.offsetY - myC.dragoffY;

			myC.valid = false; // Algo está sendo arrastado então temos que redesenhar
		}
	}, true);



	canvas.addEventListener('mousedown', function(e)
	{
		var mx = e.offsetX;
		var my = e.offsetY;

		var pictures = myC.pictures;
		var l = pictures.length; // Amount of pictures
		

		pictures.forEach(function(picture)
		{
			picture.selected = false;
		});
		

		// Para selecionar a imagem, o loop percorre todas as
		// imagens pelo array reverso em que as imagens são
		// imprimidas, então a última imagem a ser imprimida
		// está na frente de todas as outras e é a que deve ser
		// selecionada, faz o check para ver se as coordenadas
		// batem e o return; garante que o loop não continue para
		// checar outras imagens
		for(var i = l-1; i >= 0 ; i-- )
		{

			if(pictures[i].contains(mx, my))
			{
				var mySel = pictures[i];


				mySel.selected = true;
				myC.dragoffX = mx - mySel.x;
				myC.dragoffY = my - mySel.y;
				myC.dragging = true;
				myC.selection = mySel;
				myC.valid = false;



				return;
			}
		}

		//Se não retornar nada é porque não selecionou nada
		// Se tiver um objeto selecionado, desselecionamos

		if(myC.selection)
		{
			myC.selection = null;
			myC.valid = false;
		}

	}, true);
	canvas.addEventListener('mouseup',  function(e)
	{
		myC.dragging = false;
	}, true);

	this.clear = function()
	{
		this.ctx.clearRect(0, 0, this.w, this.h);
	}


	this.draw = function()
	{
		// Se algum
		if(!this.valid)
		{
			var ctx = this.ctx;
			var pictures = this.pictures;
			this.clear();

			// Adicionar aqui o que você queira que seja desenhado toda hora

			// Desenha tudo
			var l = pictures.length;
			for( var i = 0; i < l; i ++ )
			{
				var picture = pictures[i];

				// Nós podemos evitar desenhar os objetos que estão fora da tela

				if( ( picture.x > this.width ) || ( picture.y > this.height ) ||
					( picture.w < 0 ) || ( picture.h < this.height ))
					continue;

				
					pictures[i].draw(ctx);
				
			}
		

			// ** Adicionar coisas que você quieira que seja
			// desenhado o tempo todo e acime de tudo Aqui
			DrawGrid(s);


			this.valid = true;
		}
	};


};




CanvasState.prototype.addPicture = function(picture)
{
	this.pictures.push(picture);
	this.valid = false;
};

CanvasState.prototype.drawSelectBox = function(ctx, img)
{
	ctx.lineWidth = 20;
	ctx.strokeStyle = '#000';
	ctx.strokeRect(img.x, img.y, img.w, img.h);
}
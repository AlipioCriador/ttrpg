var Picture = function(img, w)
{
	this.img = img;
	this.x = img.x;
	this.y = img.y;
	this.w = w || img.width;
	this.h = img.height;
	this.selected = false;

	this.scaleBoxColor = '#88DDFF';
	this.scaleBoxSize = 20; // Tamanho dos quadradinhos que servem para
							// redimensionar a imagem
	this.scaleBoxes = [];

	if(w)
	{
		var proportion = img.width/w;
		this.h = img.height/proportion;
	}

	this.draw = function(ctx)
	{
		// Desenha tudo o que estiver aqui
		ctx.drawImage(this.img, this.x, this.y, this.w, this.h);

		// Só desenha se a imagem em questão estiver
		// selecionada
		if(this.selected)
		{

			// Cria boxes ao redor da imagem para
			// redimensionar a imagem que está selecionada
	
			var scaleBoxes = this.scaleBoxes;
			var scaleBoxSize = this.scaleBoxSize;

			var x = this.x;
			var y = this.y;
			var w = this.w;
			var h = this.h;

			/** Top Left **/
			this.scaleBoxes[0] = {
				x: x-scaleBoxSize,
				y: y-scaleBoxSize, 
				w: scaleBoxSize, 
				h: scaleBoxSize
			};
			/** Bottom Right **/
			this.scaleBoxes[1] = {
				x: x+w,
				y: y+h, 
				w: scaleBoxSize, 
				h: scaleBoxSize
			};
			/** Top Right **/
			this.scaleBoxes[2] = {
				x: x+w,
				y: y-scaleBoxSize, 
				w: scaleBoxSize, 
				h: scaleBoxSize
			};
			
			/** Bottom Left **/
			this.scaleBoxes[3] = {
				x: x-scaleBoxSize,
				y: y+h, 
				w: scaleBoxSize, 
				h: scaleBoxSize
			};
			
			


			ctx.lineWidth = 2;
			ctx.strokeStyle = 'black';
			ctx.strokeRect(this.x, this.y, this.w, this.h);

		
			this.scaleBoxes.forEach(function(box)
			{
				ctx.fillStyle = this.scaleBoxColor;
				ctx.fillRect(box.x, box.y, scaleBoxSize, scaleBoxSize);

			});
			
		}
		
	}

	

	// Retorna true se o mouse estiver dentro da imagem selecionada
	

	this.contains = function(mx, my, x, y, w, h)
	{
		var x = x || this.x;
		var y = y || this.y;
		var w = w || this.w;
		var h = h || this.h;
		return ( mx >= x ) && ( my >= y ) &&
		( mx <= x + w ) && ( my <= y + h );
	}
};
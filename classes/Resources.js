(function()
{
	var resourceCache = [];
	var loading = [];
	var readyCallbacks = [];

	//Carrega uma url de imagem ou lista de imagens
	function load(urlOrArr)
	{
		if(urlOrArr instanceof Array)
		{
			// Se for lista de imagens,
			// executa o _load um por um
			urlOrArr.forEach(function(url)
			{
				_load(url);
			});
		}
		else
		{
			// se houver apenas uma imagem
			// carrega ela
			_load(url);
		}
	}

	function _load(url)
	{
		//Se já existir um objeto com
		// o key do link que buscamos
		// não precisa carregar mais nada
		if(resourceCache[url])
		{
			return resourceCache[url];
		}
		else
		{
			var img = new Image;

			img.onload = function()
			{

				// Apóx carregar a imagen, coloca
				// o objeto Image() no key do link
				// do mesmo

				resourceCache[url] = img;
				
				// Checa no Array das imagens se existe alguma
				// chave dentro da Array que tenha como valor
				// false, e se todos os valores forem diferentes
				//de false, executa

				
				if(isReady())
				{
					readyCallbacks.forEach( function(func){ func(); });
				}

			};


			resourceCache[url] = false; 
			// Curiosamente o 'resourceCache[url] = false' roda
			// em todo o array antes do 'img.onload' rodar,
			// então o resourceCache fica com todos os links
			// antes do forEach terminar, não sei porque isso
			// acontece, mas enfim



			img.src = url;

		}
	}

	function isReady()
	{
		var ready = true;
		for( var k in resourceCache )
		{
			if( resourceCache.hasOwnProperty(k) &&
				!resourceCache[k] )
			{
				ready = false;
			}
		}
		// Eu estava cometendo o erro de colocar o return
		// dentro do for...in, não cometer esse erro novamente
		return ready;
	}

	function onReady(func)
	{
		readyCallbacks.push(func);
	}


	function get(url)
	{
		return resourceCache[url];
	}

	window.resources = 
	{
		load: load,
		onReady: onReady,
		get: get
	};
})();
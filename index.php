<!DOCTYPE html>
<html>
<head>
	<title>TTRPG</title>
</head>
<body>
	<canvas width="800" height="600"></canvas>
</body>
<script type="text/javascript" src="classes/resources.js"></script>
<script type="text/javascript" src="classes/CanvasState.js"></script>
<script type="text/javascript" src="classes/DrawGrid.js"></script>
<script type="text/javascript" src="classes/Picture.js"></script>
<script type="text/javascript" src="init.js"></script>

</html>
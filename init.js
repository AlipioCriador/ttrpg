var s;
function load()
{
	s = new CanvasState(document.getElementsByTagName('canvas')[0]);
	var images = 
	[
		'resources/Keep in Greenest2.jpg',
		'resources/max_41.png',
		'resources/max_50.png',
	];
	resources.load(images);

}

function draw()
{
	s.draw();
}


function init()
{
	
	s.addPicture(new Picture(resources.get('resources/Keep in Greenest2.jpg')));
	s.addPicture(new Picture(resources.get('resources/max_41.png'), 50));
	s.addPicture(new Picture(resources.get('resources/max_50.png')));

	

	setInterval(draw, 30);
}

load();

resources.onReady(init);